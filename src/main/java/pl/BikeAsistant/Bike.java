package pl.BikeAsistant;

import java.util.ArrayList;

/**
 * Created by Michal on 2015-01-05.
 */
public class Bike {
    private String name;
    private Integer price;
    private Integer number;
    private String photoPath;
    private ArrayList<Tag> tags;

    public Bike(String name, Integer price, Integer number, String photoPath, ArrayList<Tag> tags) {
        this.name = name;
        this.price = price;
        this.number = number;
        this.photoPath = photoPath;
        this.tags = tags;
    }

    public Bike(String name, Integer price) {
        this.name = name;
        this.price = price;
        this.tags = new ArrayList<>();
        this.number = 0;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getNumber() {
        return number;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void addTag(Tag tag){
        this.tags.add(tag);
    }
}
