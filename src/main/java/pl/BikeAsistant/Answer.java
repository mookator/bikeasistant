package pl.BikeAsistant;

/**
 * Created by Michal on 2015-01-05.
 */
public class Answer {
    private String content;
    private Integer number;

    public Answer(String content) {
        this.content = content;
        this.number=0;
    }

    public Answer(String content, Integer number) {
        this.content = content;
        this.number = number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getContent() {
        return content;
    }

    public Integer getNumber() {
        return number;
    }


}
