package pl.BikeAsistant;

/**
 * Created by Michal on 2015-01-05.
 */
public class Tag {
    private String type;
    private String content;
    private Integer number;

    public Tag(String type, String content) {
        this.type = type;
        this.content = content;
        this.number=0;
    }

    public Tag(String type, String content, Integer number) {
        this.type = type;
        this.content = content;
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}
