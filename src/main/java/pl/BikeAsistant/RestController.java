package pl.BikeAsistant;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.UUID;

/**
 * Created by Michal on 2014-12-08.
 */

@Controller
public class RestController {

    @Autowired
    DataManager dataManager;

    @Autowired
    WebSocketHandler webSocketHandler;

    @RequestMapping(value="/newId", method= RequestMethod.GET)
    public @ResponseBody
    String getNewUuid() {
        return UUID.randomUUID().toString();
    }

    @RequestMapping(value="/newList/{id}", method= RequestMethod.GET)
    public @ResponseBody
    HttpStatus getNewList(@PathVariable String id) {

        webSocketHandler.sendList(id, dataManager.getAllBikes());
        return HttpStatus.OK;
    }

    @RequestMapping(value="/answerQuestion/{questionNumber}/{answerNumber}", method= RequestMethod.GET)
    public @ResponseBody
    HttpStatus answerQuestion(@PathVariable Integer questionNumber, @PathVariable Integer answerNumber, @RequestParam String[] bikes, @RequestParam String id) {
        ArrayList<Bike> tempList=new ArrayList<>(Arrays.asList( Arrays.asList(bikes).stream().map(string -> dataManager.getBikeByNumber(Integer.parseInt(string))).toArray(Bike[]::new)));
        Tag tag;
        try{
            tag = dataManager.chooseTag(questionNumber,answerNumber);
            ArrayList<Bike> newList = dataManager.filterList(tempList,tag);
            webSocketHandler.sendList(
                    id,newList);
        } catch (NoSuchElementException e){
            webSocketHandler.sendList(
                    id,tempList);
        }

        return HttpStatus.OK;
    }

    @RequestMapping(value="/getQuestion/{previousQuestionsNumber}/{lastAnswer}", method= RequestMethod.GET)
    public @ResponseBody
    Question getNewQuestion(@PathVariable String previousQuestionsNumber, @PathVariable Integer lastAnswer) {
        return dataManager.chooseQuestion(previousQuestionsNumber, lastAnswer);
    }

    @RequestMapping(value="/getQuestion", method= RequestMethod.GET)
    public @ResponseBody
    Question getFirstQuestion() {
        return dataManager.serveQuestion(0);
    }

    @RequestMapping(value="/add/tag/{type}/{content}", method= RequestMethod.GET)
    public @ResponseBody
    Tag addNewTag(@PathVariable String type, @PathVariable String content) {
        Tag tag= new Tag(type,content);
        dataManager.addTag(tag);
        return tag;
    }

    @RequestMapping(value="/add/answer/{content}", method= RequestMethod.GET)
    public @ResponseBody
    Answer addNewAnswer( @PathVariable String content) {
        Answer answer= new Answer(content);
        dataManager.addAnswer(answer);
        return answer;
    }

    @RequestMapping(value="/add/question/{content}", method= RequestMethod.GET)
    public @ResponseBody
    Question addNewQuestion(@PathVariable String content, @RequestParam ArrayList<String> numbers) {
        Question question= new Question(content);
        dataManager.addQuestion(question, numbers);
        return question;
    }

    @RequestMapping(value="/add/qats/{questionNumber}/{answerNumber}/{tagNumber}", method= RequestMethod.GET)
    public @ResponseBody
    QATSet addNewQAT(@PathVariable Integer questionNumber, @PathVariable Integer answerNumber, @PathVariable Integer tagNumber) {
        QATSet qatSet = new QATSet(questionNumber,answerNumber);
        dataManager.addQATSet(qatSet,tagNumber);
        return qatSet;
    }

    @RequestMapping(value="/add/scenario/{prewQuestionsNumber}/{answerNumber}/{nextQuestionNumber}", method= RequestMethod.GET)
    public @ResponseBody
    Scenario addNewScenario(@PathVariable String prewQuestionsNumber, @PathVariable Integer answerNumber, @PathVariable Integer nextQuestionNumber) {
        Scenario scenario=new Scenario(prewQuestionsNumber,answerNumber,nextQuestionNumber);
        dataManager.addScenario(scenario);
        return scenario;
    }

    @RequestMapping(value="/add/bike/{name}/{price}", method= RequestMethod.GET)
    public @ResponseBody
    Bike addNewBike(@PathVariable String name, @PathVariable Integer price, @RequestParam ArrayList<String> numbers) {
        Bike bike = new Bike(name,price);
        dataManager.addBike(bike, numbers);
        return bike;
    }

    @RequestMapping(value="/save", method= RequestMethod.GET)
    public @ResponseBody
    HttpStatus saveAll(){
        dataManager.save();
        return HttpStatus.I_AM_A_TEAPOT;
    }
}
