package pl.BikeAsistant;

/**
 * Created by Michal on 2015-01-05.
 */
public class Scenario {
    private String alreadySeenNumbersOfQuestions;
    private Integer lastChosenAnswer;
    private Integer nextQuestionNumber;

    public Scenario(String alreadySeenNumbersOfQuestions, Integer lastChosenAnswer, Integer nextQuestionNumber) {
        this.alreadySeenNumbersOfQuestions = alreadySeenNumbersOfQuestions;
        this.lastChosenAnswer = lastChosenAnswer;
        this.nextQuestionNumber = nextQuestionNumber;
    }

    public Scenario() {
    }

    public Integer getNextQuestionNumber() {
        return nextQuestionNumber;
    }

    public String getAlreadySeenNumbersOfQuestions() {
        return alreadySeenNumbersOfQuestions;
    }

    public Integer getLastChosenAnswer() {
        return lastChosenAnswer;
    }
}
