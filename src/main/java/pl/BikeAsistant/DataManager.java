package pl.BikeAsistant;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Created by Michal on 2015-01-05.
 */
@Service
public class DataManager {
    private ArrayList<Tag> availableTags;
    private ArrayList<Question> availableQuestions;
    private ArrayList<Answer> allAnswers;
    private ArrayList<Bike> allBikes;

    private ArrayList<Scenario> possibleScenarios;
    private ArrayList<QATSet> possibleSets;

    FileManager fileManager = new FileManager();

    public DataManager() {
        this.availableTags= new ArrayList<>();
        this.availableQuestions= new ArrayList<>();
        this.allAnswers = new ArrayList<>();
        this.allBikes= new ArrayList<>();
        this.possibleScenarios = new ArrayList<>();
        this.possibleSets = new ArrayList<>();
        this.loadOld();
    }

    public void addTag(Tag newTag){
        boolean numberAvailable=checkNumberAvailability(newTag.getNumber(),"tag");
        if(numberAvailable){
            availableTags.add(newTag);
        } else {
            Integer newNumber=availableTags.size();
            while(!numberAvailable){
                numberAvailable=checkNumberAvailability(newNumber,"tag");
                if(numberAvailable){
                    newTag.setNumber(newNumber);
                    availableTags.add(newTag);
                } else {
                    newNumber++;
                }
            }
        }
    }
    public void addAnswer(Answer newAnswer){
        boolean numberAvailable=checkNumberAvailability(newAnswer.getNumber(),"answer");
        if(numberAvailable){
            allAnswers.add(newAnswer);
        } else {
            Integer newNumber=allAnswers.size();
            while(!numberAvailable){
                numberAvailable=checkNumberAvailability(newNumber,"answer");
                if(numberAvailable){
                    newAnswer.setNumber(newNumber);
                    allAnswers.add(newAnswer);
                } else {
                    newNumber++;
                }
            }
        }
    }
    public void addQuestion(Question newQuestion, ArrayList<String> answerNumbers){
        newQuestion=attachAnswer(newQuestion, answerNumbers);
        boolean numberAvailable=checkNumberAvailability(newQuestion.getNumber(),"question");
        if(numberAvailable){
            availableQuestions.add(newQuestion);
        } else {
            Integer newNumber=availableQuestions.size();
            while(!numberAvailable){
                numberAvailable=checkNumberAvailability(newNumber,"question");
                if(numberAvailable){
                    newQuestion.setNumber(newNumber);
                    availableQuestions.add(newQuestion);
                } else {
                    newNumber++;
                }
            }
        }
    }
    public void addQATSet(QATSet qatSet, Integer tagNumber){
        availableTags.stream().filter(tag -> tag.getNumber().equals(tagNumber)).forEach(qatSet::setTag);
        this.possibleSets.add(qatSet);
    }
    public void addScenario(Scenario scenario){
        this.possibleScenarios.add(scenario);
    }
    public void addBike(Bike bike, ArrayList<String> tagNumbers){
        boolean numberAvailable=checkNumberAvailability(bike.getNumber(),"bike");
        bike=attachTags(bike,tagNumbers);
        if(numberAvailable){
            this.allBikes.add(bike);
        } else {
            Integer newNumber=allBikes.size();
            while(!numberAvailable){
                numberAvailable=checkNumberAvailability(newNumber,"bike");
                if(numberAvailable){
                    bike.setNumber(newNumber);
                    this.allBikes.add(bike);
                } else {
                    newNumber++;
                }
            }
        }
    }

    private Bike attachTags(Bike bike,ArrayList<String> tagNumbers){
        for (String number : tagNumbers){
            availableTags.stream().filter(tag -> Integer.valueOf(number).equals(tag.getNumber())).forEach(bike::addTag);
        }
        return bike;
    }

    private Question attachAnswer(Question question,  ArrayList<String> answerNumbers){
        for (String number : answerNumbers){
            allAnswers.stream().filter(answer -> Integer.valueOf(number).equals(answer.getNumber())).forEach(question::addAnswer);
        }
        return question;
    }

    public Bike getBikeByNumber(Integer number) throws NoSuchElementException{
        for (Bike bike :allBikes){
            if(bike.getNumber().equals(number)){
                return bike;
            }
        }
        throw new NoSuchElementException();
        //return (Bike) this.allBikes.stream().filter(bike -> bike.getNumber().equals(number));
    }

    public boolean checkNumberAvailability(Integer number, String array) {
        if (array.equals("tag")) {
            for (Tag tag : availableTags) {
                if (number.equals(tag.getNumber())) {
                    return false;
                }
            }
            return true;
        } else if (array.equals("answer")) {
            for (Answer answer : allAnswers) {
                if (number.equals(answer.getNumber())) {
                    return false;
                }
            }
            return true;
        } else if (array.equals("bike")) {
            for (Bike bike : allBikes) {
                if (number.equals(bike.getNumber())) {
                    return false;
                }
            }
            return true;
        } else if (array.equals("question")) {
            for (Question question : availableQuestions) {
                if (number.equals(question.getNumber())) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private void loadOld(){
        this.availableTags= new ArrayList<>(fileManager.loadTags());
        this.availableQuestions= new ArrayList<>(fileManager.loadQuestions());
        this.allAnswers = new ArrayList<>(fileManager.loadAnswers());
        this.allBikes= new ArrayList<>(fileManager.loadBikes());
        this.possibleScenarios = new ArrayList<>(fileManager.loadScenarios());
        this.possibleSets = new ArrayList<>(fileManager.loadQATSets());

    }

    public void save(){
        fileManager.saveTags(this.availableTags);
        fileManager.saveAnswers(this.allAnswers);
        fileManager.saveBikes(this.allBikes);
        fileManager.saveQuestions(this.availableQuestions);
        fileManager.saveScenarios(this.possibleScenarios);
        fileManager.saveSets(possibleSets);
    }
    
    public Question serveQuestion(Integer questionNumber) throws NoSuchElementException{
        for(Question question : availableQuestions){
            if(question.getNumber().equals(questionNumber)){
                return question;
            }
        }
        throw new NoSuchElementException();
    }

    public Tag chooseTag(Integer questionNumber, Integer answerNumber)throws NoSuchElementException{
        for(QATSet qatSet : possibleSets){
            if(qatSet.getQuestionNumber().equals(questionNumber) && qatSet.getAnswerNumber().equals(answerNumber)){
                return qatSet.getTag();
            }
        }
        throw new NoSuchElementException();
    }

    public ArrayList<Bike> filterList(ArrayList<Bike> bikes, Tag tag){
        bikes.removeIf(bike -> !bike.getTags().stream().anyMatch(tagl -> tagl.getNumber().equals(tag.getNumber())));
        return bikes;
    }

    public Question chooseQuestion(String previousQuestionsNumber, Integer lastChosenAnswer)throws NoSuchElementException{
        for (Scenario scenario: possibleScenarios){
            if(scenario.getAlreadySeenNumbersOfQuestions().equals(previousQuestionsNumber) && scenario.getLastChosenAnswer().equals(lastChosenAnswer)){
                return serveQuestion(scenario.getNextQuestionNumber());
            }
        }
        throw new NoSuchElementException();
    }

    public ArrayList<Bike> getAllBikes() {
        return allBikes;
    }
}

