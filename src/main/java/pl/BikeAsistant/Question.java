package pl.BikeAsistant;

import java.util.ArrayList;

/**
 * Created by Michal on 2015-01-05.
 */
public class Question {
    private Integer number;
    private ArrayList<Answer> possibleAnswers;
    private String content;

    public Question(Integer number, ArrayList<Answer> possibleAnswers, String content) {
        this.number = number;
        this.possibleAnswers = possibleAnswers;
        this.content = content;
    }

    public Question(Integer number, String content) {
        this.number = number;
        this.possibleAnswers=new ArrayList<>();
        this.content = content;
    }

    public Question(String content) {
        this.number = 0;
        this.possibleAnswers=new ArrayList<>();
        this.content = content;
    }

    public void addAnswer(Answer answer){
        possibleAnswers.add(answer);
    }

    public Integer getNumber() {
        return number;
    }

    public ArrayList<Answer> getPossibleAnswers() {
        return possibleAnswers;
    }

    public String getContent() {
        return content;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
