package pl.BikeAsistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

/**
 * Created by Michal on 2014-12-08.
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Starter {

    @Bean
    DataManager dataManager(){
        return new DataManager();
    }

    @Bean
    WebSocketHandler webSocketHandler(SimpMessageSendingOperations messagingTemplate) {
        return new WebSocketHandler(messagingTemplate);
    }

    public static void main(String... args){
        SpringApplication.run(Starter.class, args);
    }
}
