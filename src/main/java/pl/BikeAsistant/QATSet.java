package pl.BikeAsistant;

/**
 * Created by Michal on 2015-01-05.
 */
public class QATSet { // Question Answer Tag set
    private Integer questionNumber;
    private Integer answerNumber;
    private Tag tag;

    public QATSet() {
    }

    public QATSet(Integer questionNumber, Integer answerNumber, Tag tag) {
        this.questionNumber = questionNumber;
        this.answerNumber = answerNumber;
        this.tag = tag;
    }

    public QATSet(Integer questionNumber, Integer answerNumber) {
        this.questionNumber = questionNumber;
        this.answerNumber = answerNumber;
        this.tag=null;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public Integer getAnswerNumber() {
        return answerNumber;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}


