package pl.BikeAsistant;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2015-01-05.
 */
public class FileManager {

    public void saveTags(ArrayList<Tag> tags){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("tags.json"), tags);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveQuestions(ArrayList<Question> questions){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("questions.json"), questions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveAnswers(ArrayList<Answer> answers){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("answers.json"), answers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveBikes(ArrayList<Bike> bikes){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("bikes.json"), bikes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveScenarios(ArrayList<Scenario> scenarios){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("scenarios.json"), scenarios);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveSets(ArrayList<QATSet> qatSets){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("qatSets.json"), qatSets);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Tag> loadTags() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("tags.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Tag[] tags = new Gson().fromJson(sb.toString(), new Tag[0].getClass());
        return ImmutableList.copyOf(tags);
    }

    public List<Question> loadQuestions() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("questions.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Question[] questions = new Gson().fromJson(sb.toString(), new Question[0].getClass());
        return ImmutableList.copyOf(questions);
    }

    public List<Answer> loadAnswers() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("answers.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Answer[] answers = new Gson().fromJson(sb.toString(), new Answer[0].getClass());
        return ImmutableList.copyOf(answers);
    }

    public List<Bike> loadBikes() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("bikes.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bike[] bikes = new Gson().fromJson(sb.toString(), new Bike[0].getClass());
        return ImmutableList.copyOf(bikes);
    }

    public List<Scenario> loadScenarios() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("scenarios.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scenario[] scenarios = new Gson().fromJson(sb.toString(), new Scenario[0].getClass());
        return ImmutableList.copyOf(scenarios);
    }

    public List<QATSet> loadQATSets() {
        InputStream is = FileManager.class.getClassLoader().getResourceAsStream("qatSets.json");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        QATSet[] qatSets = new Gson().fromJson(sb.toString(), new QATSet[0].getClass());
        return ImmutableList.copyOf(qatSets);
    }
}
