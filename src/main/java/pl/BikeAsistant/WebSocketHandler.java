package pl.BikeAsistant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import java.util.ArrayList;

/**
 * Created by Michal on 2015-01-06.
 */
public class WebSocketHandler {

    private final SimpMessageSendingOperations messagingTemplate;

    @Autowired
    public WebSocketHandler(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }


    public void sendList(String uuid, ArrayList<Bike> bikes){
        this.messagingTemplate.convertAndSend("/queue/bikelists/" + uuid, bikes);

    }
}
