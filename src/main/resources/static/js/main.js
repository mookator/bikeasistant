var app = angular.module('app', ['ui.bootstrap']);

app.service ('CommunicationService', function($http){
    this.getUuid = function(fn){
        $http({
              method: 'GET',
              url: window.location.pathname+"newId"
                        }).
                        success(function (data, status, headers, config) {
                           fn(data);
                        }).
                           error(function (data, status, headers, config) {
                           alert("failed!");
                        });

    };

    this.getNewList=function(uuid) {
        $http({
                        method: 'GET',
                        url: window.location.pathname+"newList/"+uuid
                    }).
                    success(function (data, status, headers, config) {
                        console.log("New list requested")
                    }).
                    error(function (data, status, headers, config) {
                        alert("New List request failed!");
                    });

    };

    this.get0Question = function(gn){
            $http({
                  method: 'GET',
                  url: window.location.pathname+"getQuestion"
                            }).
                            success(function (data, status, headers, config) {
                               gn(data);
                            }).
                               error(function (data, status, headers, config) {
                               alert("failed!");
                            });

        };

        this.getQuestion = function(gn, prewquestions, lastAnswer){
                $http({
                      method: 'GET',
                      url: window.location.pathname+"getQuestion/"+ prewquestions + "/" + lastAnswer
                                }).
                                success(function (data, status, headers, config) {
                                   gn(data);
                                }).
                                   error(function (data, status, headers, config) {
                                   alert("failed!");
                                });

            };

    this.answerQuestion = function(questionNumber, answerNumber, list, uuid) {
                $http({
                    method: 'Get',
                    url: window.location.pathname+"answerQuestion/"+questionNumber+"/"+answerNumber,
                    params: {bikes : list,
                             id : uuid}
                }).
                success(function (data, status, headers, config) {
                    console.log("Question answered")
                }).
                error(function (data, status, headers, config) {
                    alert("failed!");
                });
            };

        this.getNextQuestion = function(gn, previousQuestions, lastAnswer){
                $http({
                      method: 'GET',
                      url: window.location.pathname+"getQuestion/"+previousQuestions+"/"+lastAnswer
                                }).
                                success(function (data, status, headers, config) {
                                   gn(data);
                                }).
                                   error(function (data, status, headers, config) {
                                   alert("failed!");
                                });

            };
});

app.controller('Controller',['$scope' , '$location' , 'CommunicationService', function($scope, $location, CommunicationService){

    $scope.uuid;
    $scope.question;
    $scope.bikes=[];
    $scope.tempBikes=[];

    $scope.availableQuestions=false;
    $scope.questionNumber=0;
    $scope.previousQuestions="";

    var fn = function(data){
            $scope.uuid=data;
            $scope.connect();
            var args=data;
        };
    var gn = function(data){
                $scope.questionNumber+=1;
                $scope.question=data;
                $scope.availableQuestions=true;
                $scope.previousQuestions+= ""+ $scope.question.number;
            };

    $scope.init=function(){
    CommunicationService.getUuid(fn);
    $scope.get0Question();
    };

    $scope.get0Question=function(){
        CommunicationService.get0Question(gn);
    }

    $scope.convertBikes=function(){
        var bikesConverted=[];
        for(var i=0; i< $scope.bikes.length;i++){
            bikesConverted.push($scope.bikes[i].number)
        }
        return bikesConverted;
    }


    $scope.answerQuestion= function(questionNumber,answerNumber){
        $scope.availableQuestions=false;
        CommunicationService.answerQuestion(questionNumber,answerNumber, $scope.convertBikes(), $scope.uuid);
        CommunicationService.getQuestion(gn,$scope.previousQuestions,answerNumber);
        if($scope.previousQuestions==="0" || $scope.previousQuestions ==="0218" ){
            $scope.previousQuestions+=answerNumber;
        }
    };

    $scope.connect=function () {
        var socket = new SockJS(window.location.pathname+'file');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame) {

         console.log('Connected: ' + frame);
         stompClient.subscribe(/*window.location.pathname +*/'/queue/bikelists/' + $scope.uuid, function(msg){
           console.log(msg);
           $scope.$apply(function () {
              $scope.tempBikes=msg.body;
              $scope.convertResponse();
           });
         });
         CommunicationService.getNewList($scope.uuid);
        });
    }

    $scope.disconnect=function() {
       stompClient.disconnect();
       console.log("Disconnected");
    };

    $scope.convertResponse= function () {
        $scope.bikes=angular.fromJson($scope.tempBikes)
         $scope.tempBikes=[];
      };
}]);






